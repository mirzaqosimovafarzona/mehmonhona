package uz.pdp.platforma10_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Platforma101Application {

    public static void main(String[] args) {
        SpringApplication.run(Platforma101Application.class, args);
    }

}
