package uz.pdp.platforma10_1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.platforma10_1.entity.Hotel;

public interface HotelRepo extends JpaRepository<Hotel, Long> {




}
